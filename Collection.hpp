#include <vector>
#include <iostream>

template<typename Object>
const int Collection<Object>::RESERVED_DEFAULT_SIZE = 5;
template<typename Object>
Collection<Object>::Collection() {
	objectArray = new Object[RESERVED_DEFAULT_SIZE];
	reserved_size = RESERVED_DEFAULT_SIZE;
	size = 0;
}
template<typename Object>
Collection<Object>::~Collection() {
	delete[] objectArray;
}
template<typename Object>
bool Collection<Object>::isEmpty() const {
	return size == 0;
}
template<typename Object>
void Collection<Object>::makeEmpty() {
	delete[] objectArray;
	size = 0;
	objectArray = new Object[RESERVED_DEFAULT_SIZE];
}
template<typename Object>
void Collection<Object>::insert(Object const & obj) {
	if (size == reserved_size) {
		Object * new_arr = new Object[reserved_size * 2];
		for (int i = 0; i != reserved_size; i++) {
			new_arr[i] = objectArray[i]; //copy
		}
		delete[] objectArray;
		objectArray = new_arr;
		reserved_size = reserved_size * 2;
	}
	objectArray[size++] = obj;
}
template<typename Object>
void Collection<Object>::remove(Object const & obj) {
	//Object * new_arr = new Object[reserved_size];
	std::vector<int> rm_indexes;
	for (int i = 0; i != size; i++) {
		if (objectArray[i] == obj) {
			rm_indexes.push_back(i);
		}
	}
	int count = 0;
	for (int index : rm_indexes) {
		index -= count;
		for (int i = index; i < size; i++) {
			objectArray[i] = objectArray[i + 1];
		}
		size--;
		count++;
	}
}
template<typename Object>
bool Collection<Object>::contains(Object const & obj) {
	bool found = false;
	for (int i = 0; i < size; i++) {
		if (objectArray[i] == obj)
			found = true;
	}
	return found;
}
template<typename Object>
void Collection<Object>::print() const{
	if(size == 0){
		std::cout << "empty!" << std::endl;
		return;
	}
	for(int i = 0;i < size;i++){
		std::cout << objectArray[i] << " ";
	}
	std::cout << std::endl;
}
