/*
 * main.cpp
 *
 *  Created on: 2017-02-09
 *      Author: etudiant
 */
#include "Collection.h"
#include <cassert>

int main(){
	Collection<int> col{};
	assert(col.isEmpty() == true);
	col.insert(5);col.insert(-3);col.insert(5);col.insert(4);
	col.print();

	assert(col.isEmpty() == false);

	assert(col.contains(5) == true);assert(col.contains(-3) == true);
	assert(col.contains(4) == true);assert(col.contains(-5) == false);

	col.remove(-3);
	col.print();
	col.remove(5);
	col.print();

	col.remove(8574);
	col.print();
	assert(col.contains(4) == true);

	col.remove(4);
	assert(col.isEmpty() == true);
	col.print();


	col.insert(5);col.insert(-3);col.insert(5);col.insert(4);
	col.insert(5);col.insert(-3);col.insert(5);col.insert(4);
	col.insert(5);col.insert(-3);col.insert(5);col.insert(4);
	col.print();
	col.remove(4);
	col.print();
	col.makeEmpty();
	col.print();
	assert(col.isEmpty() == true);


	return 0;
}
