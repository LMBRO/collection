/*
 * Collection.h
 *
 *  Created on: 2017-02-09
 *      Author: etudiant
 */

#ifndef COLLECTION_H_
#define COLLECTION_H_

template <typename Object>
class Collection{
public:
	Collection();
	~Collection();
	bool isEmpty() const;
	void makeEmpty();
	void insert(Object const &);
	void remove(Object const &);
	bool contains(Object const &);
	void print() const;
private:
	Object * objectArray;
	int size;
	int reserved_size;
	static const int RESERVED_DEFAULT_SIZE;
};

#include "Collection.hpp"

#endif /* COLLECTION_H_ */
